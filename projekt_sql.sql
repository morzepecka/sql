--Utworzenie bazy danych --
CREATE DATABASE sklep_odziezowy;
-- Utworzenie tabeli z dostawcami --
CREATE TABLE dostawca (
id_producenta INTEGER PRIMARY KEY,
nazwa_producenta TEXT NOT NULL,
adres_producenta TEXT NOT NULL,
nip_producenta VARCHAR(10) NOT NULL,
data_podpisania_umowy DATE NOT NULL);

-- Utworzenie tabeli z produktami i powiązanie jej z tabelą dostawcy --
CREATE TABLE produkt (
id_produktu INTEGER PRIMARY KEY,
id_producenta INTEGER REFERENCES dostawca (id_producenta),
nazwa_producenta TEXT NOT NULL,
nazwa_produktu TEXT NOT NULL,
opis_produktu TEXT NOT NULL,
cena_netto_zakupu NUMERIC(8,2) CHECK (cena_netto_zakupu>=0.00),
cena_brutto_zakupu NUMERIC(8,2) CHECK (cena_brutto_zakupu>=0.00),
cena_netto_sprzedazy NUMERIC(8,2) CHECK (cena_netto_sprzedazy>=0.00),
cena_brutto_sprzedazy NUMERIC(8,2) CHECK (cena_brutto_sprzedazy>=0.00),
procent_VAT_sprzedazy DECIMAL(5,2) CHECK (procent_VAT_sprzedazy BETWEEN 0.00 AND 1.00)
);

--Utworzenie tabeli klient--
CREATE TABLE klient (
id_klienta INTEGER PRIMARY KEY, 
imie TEXT NOT NULL, 
nazwisko TEXT NOT NULL,
adres TEXT NOT NULL
);

-- Utworzenie tabeli z zamówieniami i powiązanie jej z klientami i produktami --
CREATE TABLE zamowienie (
id_zamowienia INTEGER PRIMARY KEY,
id_klienta INTEGER REFERENCES klient (id_klienta),
id_produktu INTEGER REFERENCES produkt(id_produktu),
data_zamowienia DATE NOT NULL
);

-- Uzupełnienie tabeli z dostawcami --
INSERT INTO dostawca (id_producenta, nazwa_producenta, adres_producenta, nip_producenta, data_podpisania_umowy)
VALUES (1, 'LPP SA', 'ul.Łąkowa 39/44; 80-769 Gdańsk', 5831014898, '2005-05-01');
INSERT INTO dostawca (id_producenta, nazwa_producenta, adres_producenta, nip_producenta, data_podpisania_umowy)
VALUES (2, 'CCC SA', 'ul. Strefowa 6; 59-101 Polkowice', 6922200609, '2008-08-01');
INSERT INTO dostawca (id_producenta, nazwa_producenta, adres_producenta, nip_producenta, data_podpisania_umowy)
VALUES (3, 'Kazar Group Sp z o.o.', 'ul.Lwowska 154; 37-700 Przemyśl', 7951532361, '2012-10-01');
INSERT INTO dostawca (id_producenta, nazwa_producenta, adres_producenta, nip_producenta, data_podpisania_umowy)
VALUES (4, 'VRG SA', 'Pilotów 10 31-462 Kraków', 6750000361, '2018-02-01');


-- Uzupełnienie tabeli z produktami --
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(1, 1, 'LPP SA', 'spodnie', 'czarne spodnie damskie', 50, 61.5, 70, 86.1, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(2, 1, 'LPP SA', 'koszula', 'niebieska koszula damska', 40, 49.2, 50, 61.5, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(3, 1, 'LPP SA', 'torebka', 'czerwona torebka damska', 60, 73.8, 70, 86.1, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(4, 1, 'LPP SA', 'sukienka', 'różowa sukienka', 80, 98.4, 90, 110.7, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(5, 1, 'LPP SA', 'spódnica', 'kratkowana spódnica', 50, 61.5, 65, 79.95, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(6, 2, 'CCC SA', 'kozaki', 'dugie kozaki na szpilce', 90, 110.7, 105, 129.15, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(7, 2, 'CCC SA', 'tenisówki', 'białe sportowe tenisówki', 40, 49.2, 50, 61.5, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(8, 2, 'CCC SA', 'baleriny', 'czarne płaskie baleriny', 30, 36.9, 40, 49.2, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(9, 2, 'CCC SA', 'czułenka', 'czerwone czułenka na słupku', 80, 98.4, 95, 116.85, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(10, 2, 'CCC SA', 'klapki', 'niebieskie klapki w pasy', 20, 24.6, 30, 36.9, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(11, 3, 'Kazar Group Sp z o.o.', 'kozaki', 'skórzane czerwone kozaki na szpilce', 100, 123, 120, 147.6, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(12, 3, 'Kazar Group Sp z o.o.', 'sandaki', 'różowe sandałki na słupku', 80, 98.4, 100, 123, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(13, 3, 'Kazar Group Sp z o.o.', 'trampki', 'czarne trampki w pasy', 50, 61.5, 70, 86.1, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(14, 3, 'Kazar Group Sp z o.o.', 'baleriny', 'brązowe baleriny', 60, 73.8, 75, 92.25, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(15, 3, 'Kazar Group Sp z o.o.', 'szpilki', 'czarne buty na szpilce', 90, 110.7, 110, 135.3, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(16, 4, 'VRG SA', 'koszula', 'biaa koszula mska', 80, 98.4, 100, 123, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(17, 4, 'VRG SA', 'marynarka', 'czarna marynarka męska', 110, 135.3, 130, 159.9, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(18, 4, 'VRG SA', 'spodnie', 'granatowe spodnie męskie', 100, 123, 120, 147.6, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(19, 4, 'VRG SA', 'krawat', 'krawat slim w pasy', 50, 61.5, 70, 86.1, 0.23);
INSERT INTO produkt (id_produktu, id_producenta, nazwa_producenta, nazwa_produktu, opis_produktu, cena_netto_zakupu, cena_brutto_zakupu, cena_netto_sprzedazy, cena_brutto_sprzedazy, procent_vat_sprzedazy)
VALUES(20, 4, 'VRG SA', 'skarpety', 'zielone skarpety w grochy', 40, 49.2, 60, 73.8, 0.23);

-- Uzupełnienie tabeli z klientami --
INSERT INTO klient (id_klienta, imie, nazwisko, adres)
VALUES (1, 'Adam', 'Mickiewicz', 'ul. Pańska 12/6; 01-267 Warszawa');
INSERT INTO klient (id_klienta, imie, nazwisko, adres)
VALUES (2, 'Maria', 'Skadowska-Curie', 'ul. Franciszkańska 10/6; 30-015 Kraków');
INSERT INTO klient (id_klienta, imie, nazwisko, adres)
VALUES (3, 'Bolesaw','Prus', 'ul. Chłopska 26/13; 00-123 Warszawa');
INSERT INTO klient (id_klienta, imie, nazwisko, adres)
VALUES (4, 'Jan', 'Brzechwa', 'ul. Morska 37/40; 80-345 Gdańsk');
INSERT INTO klient (id_klienta, imie, nazwisko, adres)
VALUES (5, 'Aleksander', 'Wielki', 'ul. Wiktoriańska 8; 31-500 Kraków');
INSERT INTO klient (id_klienta, imie, nazwisko, adres)
VALUES (6, 'Eliza', 'Orzeszkowa', 'ul.Dolna 13; 02-585 Warszawa');
INSERT INTO klient (id_klienta, imie, nazwisko, adres)
VALUES (7, 'Zofia', 'Nałkowska', 'ul.Graniczna 50/3; 80-345 Gdańsk');
INSERT INTO klient (id_klienta, imie, nazwisko, adres)
VALUES (8, 'Henryk', 'Sienkiewicz', 'ul.Potopu 13; 02-645 Warszawa');
INSERT INTO klient (id_klienta, imie, nazwisko, adres)
VALUES (9, 'Juliusz', 'Słowacki', 'ul. Kordiańska 16; 31-500 Kraków');
INSERT INTO klient (id_klienta, imie, nazwisko, adres)
VALUES (10, 'Jan', 'Kochanowski', 'ul.Mickiewicza 50/3; 80-345 Gdańsk');


-- Uzupełnienie tabeli z zamówieniami --
INSERT INTO zamowienie (id_zamowienia, id_klienta, id_produktu, data_zamowienia)
VALUES (1, 5, 16, '2019-03-31');
INSERT INTO zamowienie (id_zamowienia, id_klienta, id_produktu, data_zamowienia)
VALUES (2, 8, 18, '2019-04-15');
INSERT INTO zamowienie (id_zamowienia, id_klienta, id_produktu, data_zamowienia)
VALUES (3, 2, 5, '2019-05-03');
INSERT INTO zamowienie (id_zamowienia, id_klienta, id_produktu, data_zamowienia)
VALUES (4, 9, 8, '2019-06-10');
INSERT INTO zamowienie (id_zamowienia, id_klienta, id_produktu, data_zamowienia)
VALUES (5, 3, 13, '2019-07-21');
INSERT INTO zamowienie (id_zamowienia, id_klienta, id_produktu, data_zamowienia)
VALUES (6, 1, 3, '2019-08-05');
INSERT INTO zamowienie (id_zamowienia, id_klienta, id_produktu, data_zamowienia)
VALUES (7, 4, 20, '2019-09-19');
INSERT INTO zamowienie (id_zamowienia, id_klienta, id_produktu, data_zamowienia)
VALUES (8, 10, 10, '2019-10-13');
INSERT INTO zamowienie (id_zamowienia, id_klienta, id_produktu, data_zamowienia)
VALUES (9, 6, 4, '2019-11-01');
INSERT INTO zamowienie (id_zamowienia, id_klienta, id_produktu, data_zamowienia)
VALUES (10, 7, 11, '2019-12-20');

-- Wszystkie produkty od dostawcy z pozycji 1. z danymi o tym dostawcy --
SELECT * FROM produkt p 
JOIN dostawca d ON p.id_producenta = d.id_producenta 
WHERE d.id_producenta = 1;

-- Wszystkie produkty od dostawcy z pozycji 1. z danymi o tym dostawcy posortowane A-Z wg nazwy--
SELECT * FROM produkt p 
JOIN dostawca d ON p.id_producenta = d.id_producenta 
WHERE d.id_producenta = 1
ORDER BY p.nazwa_produktu ASC;

-- Średnia cena za produkt od dostawcy z pozycji 1. --
SELECT AVG(cena_brutto_zakupu) FROM produkt
WHERE id_producenta = 1
GROUP BY id_producenta;

-- Podział produktów od dostawcy z pozycji 1. na dwie grupy: tanie i drogie--
SELECT id_produktu, nazwa_produktu, opis_produktu, cena_brutto_zakupu, 
CASE WHEN 
cena_brutto_zakupu < (SELECT AVG(cena_brutto_zakupu) FROM produkt WHERE id_producenta = 1 GROUP BY id_producenta) THEN 'Tanie' 
ELSE 'Drogie' END AS grupa
FROM produkt
WHERE id_producenta =1;

--Nazwy zamówionych produktów--
SELECT p.nazwa_produktu FROM zamowienie z 
JOIN produkt p ON z.id_produktu = p.id_produktu;

-- Wszystkie zamówione produkty na pierwszych pieciu pozycjach --
SELECT * FROM produkt p2 
JOIN zamowienie z2 ON z2.id_produktu = p2.id_produktu
WHERE z2.id_zamowienia BETWEEN 1 AND 5
ORDER BY z2.id_zamowienia;

--Wartość wszystkich zamówień --
SELECT SUM(p.cena_brutto_sprzedazy) FROM zamowienie z
JOIN produkt p ON z.id_produktu = p.id_produktu;

-- Wszystkie zamówienia z produktami posortowane wg daty od najstarszego do najnowszego --
SELECT * FROM zamowienie z2
JOIN produkt p ON z2.id_produktu = p.id_produktu
ORDER BY z2.data_zamowienia;

-- Pozycje z brakującymi danymi w tabeli produkt --
SELECT * FROM produkt p 
WHERE id_produktu IS NULL OR id_producenta IS NULL 
OR nazwa_producenta IS NULL OR nazwa_produktu IS NULL OR opis_produktu IS NULL 
OR cena_netto_zakupu IS NULL OR cena_brutto_zakupu IS NULL 
OR cena_netto_sprzedazy IS NULL OR cena_brutto_sprzedazy IS NULL 
OR procent_vat_sprzedazy IS NULL;

-- Najczęściej sprzedawane produkty wraz z ceną --
   -- tabela do obliczenia średniej liczby zamówień per produkt --
WITH czestosci_zamowien AS 
(SELECT p2.id_produktu, p2.nazwa_produktu, COUNT(z2.id_zamowienia) liczba_zamowien FROM produkt p2
LEFT JOIN zamowienie z2 ON z2.id_produktu = p2.id_produktu
GROUP BY p2.id_produktu, p2.nazwa_produktu)
  -- produkty zamawiane częściej niż średnio --
SELECT p.id_produktu, p.nazwa_produktu, p.cena_brutto_sprzedazy, COUNT(z.id_zamowienia) liczba_zamowien FROM produkt p
LEFT JOIN zamowienie z ON z.id_produktu = p.id_produktu
GROUP BY p.id_produktu, p.nazwa_produktu, p.cena_brutto_sprzedazy 
HAVING COUNT(z.id_zamowienia)> (SELECT AVG(liczba_zamowien) FROM czestosci_zamowien)
ORDER BY COUNT(z.id_zamowienia) DESC;

-- Dzień z największą liczbą zamówień (spośród dni, w których były zamówienia) --
 --- tabela z liczbą zamówień per dzień (spośród tych, w których były zamówienia)--
WITH czestosc_zamowien_1 AS 
(SELECT DATE_PART('day',data_zamowienia) dzien, COUNT(id_zamowienia) liczba_zamowien FROM zamowienie z2 
GROUP BY DATE_PART('day',data_zamowienia))
 --- dzien/dni, w których liczba zamówień osiga największą wartość --
SELECT dzien FROM czestosc_zamowien_1
WHERE liczba_zamowien = (SELECT MAX(liczba_zamowien) FROM czestosc_zamowien_1);




